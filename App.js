import React,{ useEffect, useState, Component } from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


import Login from './screens/Login';
import Productos from './screens/Productos';
import Citas from './screens/Citas';
import Usuarios from './screens/Usuarios';
import Principal from './screens/Principal';
import DetalleP from './screens/DetalleP';
import RegistrarUsuario from './screens/RegistarUsuario';
import EditarCC from './screens/EditarCC';
import EditarCiA from './screens/EditarCiA';
import RegistrarProducto from './screens/RegistrarProducto';
import PrincipalC from './screens/PrincipalC';
import ProductosC from './screens/ProductoC';
import AsyncStorage from "@react-native-async-storage/async-storage";
import 'react-native-gesture-handler';

import Loader from './screens/Loader';
import RegistrarCita from "./screens/RegistrarCita";
import Servicios from "./screens/Servicios";
import Inventario from "./screens/Inventario";
import ServiciosC from "./screens/ServiciosC";
import RegistrarCitaC from "./screens/RegistrarCitarC";
import PrincipalE from "./screens/PrincipalE";
import ServiciosE from "./screens/ServiciosE";
import CitasE from "./screens/CitasE";
import RegistrarCitaE from "./screens/RegistrarCitaE";
import EditarServicios from "./screens/EditarServicio";
import EditarInventario from "./screens/EditarInventario";
import EditarUsuario from "./screens/EditarUsuario";
import EditarProducto from "./screens/EditarProducto";

const Stack = createStackNavigator();

export default function App() {

  const [data, setData] = useState();

  function postData() {

    const response =  fetch('https://jsonplaceholder.typicode.com/users', {
    method: 'GET',
    headers: {'Content-Type': 'application/json'}
    }).then((response) => response.json())
    .then((responseJson) => {
      setData(responseJson);
    })
    .catch((error) => {
      console.error(error);
    });
  }

  const [cargando, setCargando] = useState(false);
  useEffect(() => {

    const firstLoad = async () => {
      //postData()
      setCargando(true);
      try {
        fetch('https://jsonplaceholder.typicode.com/users', {
          method: 'GET',
          headers: {'Content-Type': 'application/json'}
          }).then((response) => response.json())
          .then((responseJson) => {
            setData(responseJson)
          })
          .catch((error) => {
            console.error(error);
          });

        await AsyncStorage.setItem('USUARIOS', JSON.stringify(data));

        await AsyncStorage.setItem('PRODUCTOS', JSON.stringify([
          {"id":"1","brand":"covergirl","name":"America crew","price":"10.99", "image_link":"https://m.media-amazon.com/images/I/61UbGHvMIOL._AC_SX679_.jpg","stock": "20"},
          {"id":"2","brand":"covergirl","name":"Afeitadora","price":"12.99",  "image_link":"https://m.media-amazon.com/images/I/51bo7xPi7jL._AC_SX679_.jpg","stock": "20"},
          {"id":"3","brand":"covergirl","name":"Tinte barba","price":"12.99","image_link":"https://images-na.ssl-images-amazon.com/images/I/81VhVac7eOL.__AC_SX300_SY300_QL70_ML2_.jpg","stock": "20"},
          {"id":"4","brand":"colourpop","name":"Lippie Pencil","price":"5.0","image_link":"https://cdn.shopify.com/s/files/1/1338/0845/collections/lippie-pencil_grande.jpg?v=1512588769","stock": "20"}
        ]) );
        await AsyncStorage.setItem('CITAS', JSON.stringify([
          { "id":"1", "usuario": 'Leanne Graham', "horario": '10:30', "servicio": 'Corte de cabello',"fecha": '10-04-2022'},
          { "id": "2", "usuario": 'Kurtis Weissnat', "horario": '11:00', "servicio": 'Facil',"fecha": '10-04-2022'},
          { "id": "3","usuario": "Nicholas Runolfsdottir", "horario": '12:00', "servicio": 'Afeitado de barba',"fecha": '10-04-2022'},
          { "id": "4", "usuario": 'Kurtis Weissnat', "horario": '13:00', "servicio": 'Spa',"fecha": '10-04-2022'}]
        ) );

        await AsyncStorage.setItem('CITASC', JSON.stringify([
          { "id":"1", "usuario": 'Leanne Graham', "horario": '10:30', "servicio": 'Corte de cabello',"fecha": '10-04-2022'}]
        ) );

        await AsyncStorage.setItem('SERVICIOS', JSON.stringify([
          { "id":"1", "servicio": 'Corte de cabello',"precio": "120"},
          { "id": "2", "servicio": 'Spa', "precio": '600'}]
        ) );


        } catch (err) {
          console.log(err);
      }
      setCargando(false);
    }

    firstLoad();

  }, []);
  return cargando ? <Loader
  mensaje="Cargando..."/> : (
    <NavigationContainer>
      <Stack.Navigator
      screenOptions={{
        headerStyle : {
          backgroundColor: '#FFF'
        },
        headerTintColor: '#3086F7'
      }}
      >
        {/* 
        Representamos cada screen de RN
        como un Stack.Screen
        */}
        <Stack.Screen
          name='Login'
          component={Login}
          options = {{title : ''}}
        />
         <Stack.Screen
          name='Productos'
          component={Productos}
          options = {{title : 'Productos'}}
        />
        <Stack.Screen
         name='Registrar producto'
         component={RegistrarProducto}
         options = {{title : 'Productos'}}
       />
        <Stack.Screen
         name='Citas'
         component={Citas}
         options = {{title : 'Citas'}}
       />
        <Stack.Screen
          name='Usuarios'
          component={Usuarios}
          options = {{title : 'Usuarios'}}
        />
        <Stack.Screen
          name='Registrar usuario'
          component={RegistrarUsuario}
          options = {{title : 'Usuarios'}}
        />
        <Stack.Screen
        name='Principal'
        component={Principal}
        options = {{title : 'BarberyShop'}}
        />
        
        <Stack.Screen
        name='PrincipalC'
        component={PrincipalC}
        options = {{title : 'BarberyShop'}}
        />
      
        <Stack.Screen
          name='Editar'
          component={DetalleP}
          options = {{title : 'BarberyShop'}}
        />
        <Stack.Screen
          name='Editar cita admin'
          component={EditarCiA}
          options = {{title : 'BarberyShop'}}
        />  
        <Stack.Screen
        name='Editar cita cliente'
        component={EditarCC}
        options = {{title : 'BarberyShop'}}
      /> 
         <Stack.Screen
          name='ProductosC'
          component={ProductosC}
          options = {{title : 'BarberyShop'}}
        /> 
        
        <Stack.Screen
        name='Editar cita'
        component={EditarCiA}
        options = {{title : 'BarberyShop'}}
        />
        <Stack.Screen
        name='Registrar cita'
        component={RegistrarCita}
        options = {{title : 'BarberyShop'}}
        />
        <Stack.Screen
        name='Servicios'
        component={Servicios}
        options = {{title : 'BarberyShop'}}
        />

        <Stack.Screen
        name='Inventario'
        component={Inventario}
        options = {{title : 'BarberyShop'}}
        />
        <Stack.Screen
        name='Servicios cliente'
        component={ServiciosC}
        options = {{title : 'BarberyShop'}}
        />
        <Stack.Screen
        name='Registrar cita cliente'
        component={RegistrarCitaC}
        options = {{title : 'BarberyShop'}}
        />
        <Stack.Screen
        name='PrincipalE'
        component={PrincipalE}
        options = {{title : 'BarberyShop'}}
        />
        <Stack.Screen
        name='ServiciosE'
        component={ServiciosE}
        options = {{title : 'BarberyShop'}}
        />
        <Stack.Screen
        name='CitasE'
        component={CitasE}
        options = {{title : 'BarberyShop'}}
        />
        <Stack.Screen
        name='Registrar cita empleado'
        component={RegistrarCitaE}
        options = {{title : 'BarberyShop'}}
        />  
        <Stack.Screen
        name='Editar servicio'
        component={EditarServicios}
        options = {{title : 'BarberyShop'}}
        />  
        <Stack.Screen
        name='Editar producto'
        component={EditarProducto}
        options = {{title : 'BarberyShop'}}
        />
        <Stack.Screen
        name='Editar inventario'
        component={EditarInventario}
        options = {{title : 'BarberyShop'}}
        />
        <Stack.Screen
        name='Editar usuario'
        component={EditarUsuario}
        options = {{title : 'BarberyShop'}}
        />
  
      </Stack.Navigator>
    </NavigationContainer>
  );
}
