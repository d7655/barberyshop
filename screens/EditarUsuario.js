import React,{ useEffect, useState } from "react";
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity, ScrollView,Button
  } from "react-native";

  import AsyncStorage from "@react-native-async-storage/async-storage";
import Loader from './Loader';

export default function EditarUsuario(props){

    
    const [id, setId] = useState('');
    const [data, setData] = useState([])
    const [cargando, setCargando] = useState(false);
    const [username, setUsername] = useState('');
    const [name, setName] = useState('');
    const [tel, setTel] = useState('');
    const [correo, setCorreo] = useState('');

    
    function onUpdate(){


      const dropItem = async () => {

        const  Newdata = data
        const index = data.map(function(x) {return x.id; }).indexOf(id);
        Newdata[index].name =  name;
        Newdata[index].phone =  tel;
        Newdata[index].email =  correo;
        setData(Newdata);
        console.log(Newdata);
        try {
          await AsyncStorage.setItem('USUARIOS', JSON.stringify(data));
          props.navigation.navigate('Principal')
        } catch (err) {
          console.log(err);
        }
      };
      dropItem();
    }

  // idUser: user.id, username : user.username, name: user.name, correo: user.email, tel: user.phone 
    useEffect(() => {
    
      setId(props.route.params.id);
      setUsername(props.route.params.username);
      setName(props.route.params.name);
      setCorreo(props.route.params.correo);
      setTel(props.route.params.tel);
      const firstLoad = async () => {
        setCargando(true);
        try {    
            
          const Mdata = JSON.parse(await AsyncStorage.getItem('USUARIOS'))
          setData(Mdata);

        } catch (err) {
          console.log(err);
        }
        setCargando(false);
      };
      firstLoad();
    }, []);
    return cargando ? <Loader
    mensaje="Cargando..."/> : ( 
    <View style={forms.container}>
      <Text style={forms.uneditable}>Usuario: {username}  </Text>
      <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        value={name}
        onChangeText={setName}
      />
      <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        value={correo}
        onChangeText={setCorreo}
      />
      <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        value={tel}
        onChangeText={setTel}
      />
      <Button  title="Modificar" onPress={() => onUpdate()} />
    </View>
    );
  
}

const forms = StyleSheet.create({
    container: {
      margin: 10
    },
    alert: {
      margin: 15,
      fontSize: 20,
      fontWeight: "bold",
      textAlign: "center"
    },
    uneditable: {
      padding: 5,
      marginBottom: 10
    },
    editable: {
      padding: 5,
      height: 40,
      borderColor: "#000",
      borderWidth: 1,
      marginBottom: 10,
      backgroundColor: "white",
      borderRadius: 10,
      marginBottom: 10,
      color: "#000"
    }
  });
  
