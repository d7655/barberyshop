import React,{ useEffect, useState } from "react";
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity, ScrollView,Button
  } from "react-native";

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {FontAwesome5} from '@expo/vector-icons';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Loader from './Loader';


export default function RegistrarUsuario(props){

  const [cargando, setCargando] = useState(false);
  const [data, setData] = useState([])

  const [id, setId] = useState('');
  const [username, setUsername] = useState('');
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [pass, setPass] = useState('');

  
  var RSAKey = require('react-native-rsa');
  const bits = 1024;
  const exponent = '10001'; 
  var rsa = new RSAKey();
  var r = rsa.generate(bits, exponent);
  var publicKey = rsa.getPublicString(); 
  var privateKey = rsa.getPrivateString();
  var rsa = new RSAKey();
  rsa.setPublicString(publicKey);

  function onCreate(){
    var encrypted = rsa.encrypt(pass);
    const id = data.length + 1;
    data.push({ id: id , username : username , name : name , phone :  phone , email:  email , pass :  encrypted })
    
    const saveUser = async () => {
      
      try {
        await AsyncStorage.setItem('USUARIOS', JSON.stringify(data));
        console.log(data)
      } catch (err) {
        console.log(err);
      }
    };
    saveUser();
  }

  useEffect(() => {
    
    const firstLoad = async () => {
      setCargando(true);
      try {    
        const Mdata = JSON.parse(await AsyncStorage.getItem('USUARIOS'))
        setData(Mdata);
      } catch (err) {
        console.log(err);
      }
      setCargando(false);
    };
    firstLoad();
  }, []);

  return cargando ? <Loader
  mensaje="Cargando..."/> : (
        <View style={forms.container}> 
        <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        placeholder='Username'
        onChangeText={setUsername}
        //onChangeText={onChangeValue}
        />
        <TextInput
          style={forms.editable}
          editable
          maxLength={20}
          placeholder='Nombre'
          onChangeText={setName}
          //onChangeText={onChangeValue}
        />
        <TextInput
          style={forms.editable}
          editable
          maxLength={20}
          placeholder='Telefono'
          onChangeText={setPhone}
          //onChangeText={onChangeValue}
        />
        <TextInput
          style={forms.editable}
          editable
          maxLength={20}
          placeholder='Correo'
          onChangeText={setEmail}
         // onChangeText={(value) => onChangeDesc(value)}
        />
        
        <TextInput
            placeholder="Password"
            secureTextEntry={true}
            underlineColorAndroid="transparent"
            style={forms.editable}
            onChangeText={setPass}
        />
        <Button  title="Registrar" onPress={() =>  onCreate()} />
      </View>
      );
    
  }
  
  const forms = StyleSheet.create({
      container: {
        margin: 10
      },
      alert: {
        margin: 15,
        fontSize: 20,
        fontWeight: "bold",
        textAlign: "center"
      },
      uneditable: {
        padding: 5,
        marginBottom: 10
      },
      editable: {
        padding: 5,
        height: 40,
        borderColor: "#000",
        borderWidth: 1,
        marginBottom: 10,
        backgroundColor: "white",
        borderRadius: 10,
        marginBottom: 10,
        color: "#000"
      }
    });
    
  