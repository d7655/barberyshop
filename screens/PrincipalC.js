import React,{ useEffect, useState, Component } from "react";
import { Text, View } from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {FontAwesome5} from '@expo/vector-icons';

const Tab = createBottomTabNavigator();

import CitasC from './CitasC';
import ProductosC from './ProductoC';
import ServiciosC from "./ServiciosC";

export default function PrincipalC(props){
    return(
        <Tab.Navigator
        screenOptions={({route}) => ({
            tabBarIcon :({color}) => {
                let icono;
                if(route.name == 'Citas cliente'){
                    icono = 'calendar-alt';
                }else if(route.name == 'Usuario'){
                    icono = 'user-alt';
                }else if(route.name == 'Productos cliente'){
                    icono = 'pump-soap';
                }else if(route.name == 'Servicios cliente'){
                    icono = 'cut';
                }

                return <FontAwesome5 name={icono} size={20} color={color}/>
            } 
        })
    }
    
    tabBarOptions={{
            activeTintColor: 'blue',
            inactiveTintColor: 'gray',
            showLabel: false,
        
        }}
        >
            <Tab.Screen
            name='Citas cliente'
            component={CitasC}
            options={{title: 'Citas'}}
            />
            <Tab.Screen
            name='Productos cliente'
            component={ProductosC}
            options={{title: 'Productos'}}
            />
            <Tab.Screen
            name='Servicios cliente'
            component={ServiciosC}
            options={{title: 'Servicios'}}
            />
        </Tab.Navigator>
    );

}