import React,{ useEffect, useState } from "react";
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity, ScrollView,Button,Image

  } from "react-native";
  import { LogBox } from 'react-native';

import AsyncStorage from "@react-native-async-storage/async-storage";
import {FontAwesome5} from '@expo/vector-icons';
import Loader from './Loader';
import Constants from 'expo-constants';

import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';

export default function EditarProducto(props){

    LogBox.ignoreLogs(["Require cycle:"])
    console.disableYellowBox = true;
    
    const [id, setId] = useState('');
    const [data, setData] = useState([])
    const [cargando, setCargando] = useState(false);
    const [name, setName] = useState('');
    const [price, setPrice] = useState('');
    const [image, setImagen] = useState(undefined);

    
    function onUpdate(){


      const dropItem = async () => {

        const  Newdata = data
        const index = data.map(function(x) {return x.id; }).indexOf(id);
        Newdata[index].name =  name;
        Newdata[index].price =  price;
        Newdata[index].image_link =  image;
        setData(Newdata);
        console.log(Newdata);
        try {
          await AsyncStorage.setItem('PRODUCTOS', JSON.stringify(data));
          props.navigation.navigate('Principal')
        } catch (err) {
          console.log(err);
        }
      };
      dropItem();
    }


    useEffect(() => {
    // id: item.id, name : item.name, price : item.price, image_link: item.image_link
      setId(props.route.params.id);
      setName(props.route.params.name);
      setPrice(props.route.params.price);
      setImagen(props.route.params.image_link);
      const firstLoad = async () => {
        setCargando(true);
        try {    
            
          const Mdata = JSON.parse(await AsyncStorage.getItem('PRODUCTOS'))
          setData(Mdata);

        } catch (err) {
          console.log(err);
        }
        setCargando(false);
      };
      firstLoad();
    }, []);
  
    return cargando ? <Loader
    mensaje="Cargando..."/> : ( 
    <View style={forms.container}> 
        <FontAwesome5.Button
        name='camera'
        onPress={
          async () => {
          if (Constants.platform.ios || Constants.platform.android) {
              const permisoGaleria = await Permissions.askAsync(Permissions.CAMERA_ROLL);
              if (permisoGaleria.status === 'granted') {
                  try {
                      let imagenGaleria = await ImagePicker.launchImageLibraryAsync({
                          mediaTypes: ImagePicker.MediaTypeOptions.Images,
                          allowsEditing: true,
                          aspect: [4, 3],
                          quality: 1
                      });
                      if (imagenGaleria.cancelled === false) {
                          setImagen(imagenGaleria.uri);
                      }
                      else {
                      }
                  }
                  catch (e) {
                      console.log(e);
                  }
              }
              else {
                  Alert.alert(
                      'ERROR',
                      'No contamos con el permiso para acceder a la galería'
                  )
              }
          }
        }}
        />
        {image && <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
        <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        placeholder='Nombre'
        value={name}
        onChangeText={setName}
        />

        <TextInput
            style={forms.editable}
            editable
            value={price}
            maxLength={20}
            placeholder='Precio'
            onChangeText={setPrice}
        />
        <Button  title="Registrar"  onPress={() =>  onUpdate()} />
    </View>

    );
  
}

const forms = StyleSheet.create({
    container: {
      margin: 10
    },
    alert: {
      margin: 15,
      fontSize: 20,
      fontWeight: "bold",
      textAlign: "center"
    },
    uneditable: {
      padding: 5,
      marginBottom: 10
    },
    editable: {
      padding: 5,
      height: 40,
      borderColor: "#000",
      borderWidth: 1,
      marginBottom: 10,
      backgroundColor: "white",
      borderRadius: 10,
      marginBottom: 10,
      color: "#000"
    }
  });
  
