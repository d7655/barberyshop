import React,{ useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity, ScrollView,Button
} from "react-native";
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {FontAwesome5} from '@expo/vector-icons';
import { useIsFocused } from '@react-navigation/native';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Loader from './Loader';
export default function Usuarios(props){

  const isFocused = useIsFocused();

  const [cargando, setCargando] = useState(false);
  const [data, setData] = useState([]);

  const [id, setId] = useState('');
  const [username, setUsername] = useState('');
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');

  function onDelete(id){
    setData(data.filter((c) => c.id !== id))
    const DeleteItem = async () => {
      try {
        await AsyncStorage.setItem('USUARIOS', JSON.stringify(data.filter((c) => c.id !== id)));
      } catch (err) {
        console.log(err);
      }
    };
    DeleteItem();
  }

  useEffect(() => {
    
    const firstLoad = async () => {
      setCargando(true);
      try {    
        const Mdata = JSON.parse(await AsyncStorage.getItem('USUARIOS'))
        setData(Mdata);
        console.log(Mdata);
      } catch (err) {
        console.log(err);
      }
      setCargando(false);
    };
    firstLoad();
  }, [isFocused]);

  return cargando ? <Loader
  mensaje="Cargando..."/> :(
      <ScrollView>
        <View style={lists.fixToButon}>
        <FontAwesome5.Button
              onPress={() => props.navigation.navigate("Registrar usuario")}
              name='plus'
              color="white"
              backgroundColor="green"
              size={25}
              style={buttons.primary}
             />

        </View>
        

        {data.map((user) => (
          <View style={lists.list} key={user.id}>
            <View style={lists.fixToText}>
              <View>
                <Text>Usuario: {user.username}</Text>
                <Text>Nombre: {user.name}</Text>
              </View>
            </View>
            <FontAwesome5
                  onPress={() => props.navigation.navigate("Editar usuario", { id: user.id, username : user.username, name: user.name, correo: user.email, tel: user.phone })}
                  name='edit'
                  backgroundColor='blue'
                  size={25}
                />
            <FontAwesome5
                  onPress={() => onDelete(user.id)} 
                  name='trash'
                  backgroundColor="red"
                  size={25}
                  />
            </View>
        ))}
      </ScrollView>
    
    );

}

const styles = new StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#ffff',
      alignItems: 'center',
      justifyContent: 'center',
  },
  Titulo: {
    fontSize: 19,
    color: "#000",
    fontWeight: "bold",
    marginBottom: 30,
    letterSpacing: 2
  },
  title: {
    textAlign: "center",
    marginVertical: 8
  },
  list: {
    flex: 1,
    justifyContent: "space-between",
    flexDirection: "row",
    padding: 10,
    flexWrap: "wrap"
  },
  fixToText: {
    width: "50%"
  },
  fixToButon: {
    width: "20%"

  },
  separator: {
    marginVertical: 8,
    borderBottomColor: "#737373",
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  scrollView: {
    marginHorizontal: 5
  }
});

const forms = StyleSheet.create({
  container: {
    margin: 10
  },
  alert: {
    margin: 15,
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center"
  },
  uneditable: {
    padding: 5,
    marginBottom: 10
  },
  editable: {
    padding: 5,
    height: 5,
    width: 5,
    borderColor: "gray",
    borderWidth: 1,
    marginBottom: 10,
    backgroundColor: "white"
  }
});

const buttons = StyleSheet.create({
  primary: {
    flex: 1,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 5,
    marginRight: 5
  }
});

const lists = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: "left",
    marginHorizontal: 8,
    marginTop: 10,
  },
  title: {
    textAlign: "center",
    marginVertical: 8
  },
  list: {
    flex: 1,
    justifyContent: "space-between",
    flexDirection: "row",
    padding: 10,
    flexWrap: "wrap"
  },
  fixToText: {
    width: "60%"
  },
  separator: {
    marginVertical: 4,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  scrollView: {
    marginHorizontal: 5
  }
});