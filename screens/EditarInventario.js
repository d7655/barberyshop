import React,{ useEffect, useState } from "react";
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity, ScrollView,Button
  } from "react-native";

  import AsyncStorage from "@react-native-async-storage/async-storage";
import Loader from './Loader';

export default function EditarInventario(props){

    
    const [id, setId] = useState('');
    const [data, setData] = useState([])
    const [cargando, setCargando] = useState(false);
    const [name, setName] = useState('');
    const [stock, setStock] = useState('');

    
    function onUpdate(){


      const dropItem = async () => {

        const  Newdata = data
        const index = data.map(function(x) {return x.id; }).indexOf(id);
        Newdata[index].name =  name;
        Newdata[index].stock =  stock;
        setData(Newdata);
        console.log(Newdata);
        try {
          await AsyncStorage.setItem('PRODUCTOS', JSON.stringify(data));
          props.navigation.navigate('Principal')
        } catch (err) {
          console.log(err);
        }
      };
      dropItem();
    }

    useEffect(() => {
    
      setId(props.route.params.id);
      setName(props.route.params.name);
      setStock(props.route.params.stock);
      const firstLoad = async () => {
        setCargando(true);
        try {    
            
          const Mdata = JSON.parse(await AsyncStorage.getItem('PRODUCTOS'))
          setData(Mdata);

        } catch (err) {
          console.log(err);
        }
        setCargando(false);
      };
      firstLoad();
    }, []);
  
    return cargando ? <Loader
    mensaje="Cargando..."/> : ( 
    <View style={forms.container}> 

        <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        placeholder='Nombre'
        value={name}
        onChangeText={setName}
        />
        
        <TextInput
            placeholder="Stock"
            editable
            maxLength={20}
            underlineColorAndroid="transparent"
            style={forms.editable}
            value={stock}
            onChangeText={setStock}
        />
        <Button  title="Registrar"  onPress={() =>  onUpdate()} />
    </View>

    );
  
}

const forms = StyleSheet.create({
    container: {
      margin: 10
    },
    alert: {
      margin: 15,
      fontSize: 20,
      fontWeight: "bold",
      textAlign: "center"
    },
    uneditable: {
      padding: 5,
      marginBottom: 10
    },
    editable: {
      padding: 5,
      height: 40,
      borderColor: "#000",
      borderWidth: 1,
      marginBottom: 10,
      backgroundColor: "white",
      borderRadius: 10,
      marginBottom: 10,
      color: "#000"
    }
  });
  
