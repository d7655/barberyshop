import React,{ useEffect, useState } from "react";
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity, ScrollView,Button
  } from "react-native";

  import AsyncStorage from "@react-native-async-storage/async-storage";
import Loader from './Loader';

export default function EditarCC(props){

    
    const [id, setId] = useState('');
    const [data, setData] = useState([])
    const [cargando, setCargando] = useState(false);
    const [username, setUsername] = useState('');
    const [horario, setHorario] = useState('');
    const [fecha, setfecha] = useState('');
    const [servicio, setServicio] = useState('');

    
    function onUpdate(){


      const dropItem = async () => {

        const  Newdata = data
        const index = data.map(function(x) {return x.id; }).indexOf(id);
        Newdata[index].horario =  horario;
        Newdata[index].fecha =  fecha;
        Newdata[index].servicio =  servicio;
        setData(Newdata);
        console.log(Newdata);
        try {
          await AsyncStorage.setItem('CITASC', JSON.stringify(data));
          props.navigation.navigate('PrincipalC')
        } catch (err) {
          console.log(err);
        }
      };
      dropItem();
    }

    useEffect(() => {
    
      setId(props.route.params.id);
      setUsername(props.route.params.usuario);
      setHorario(props.route.params.horario);
      setfecha(props.route.params.fecha);
      setServicio(props.route.params.servicio);
      const firstLoad = async () => {
        setCargando(true);
        try {    
            
          const Mdata = JSON.parse(await AsyncStorage.getItem('CITASC'))
          setData(Mdata);

        } catch (err) {
          console.log(err);
        }
        setCargando(false);
      };
      firstLoad();
    }, []);
  
    return cargando ? <Loader
    mensaje="Cargando..."/> : ( 
    <View style={forms.container}>
      <Text style={forms.uneditable}>Usuario: {username}  </Text>
      <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        value={horario}
        onChangeText={setHorario}
      />
      <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        value={fecha}
        onChangeText={setfecha}
      />
      <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        value={servicio}
        onChangeText={setServicio}
      />
      <Button  title="Modificar" onPress={() => onUpdate()} />
    </View>
    );
  
}

const forms = StyleSheet.create({
    container: {
      margin: 10
    },
    alert: {
      margin: 15,
      fontSize: 20,
      fontWeight: "bold",
      textAlign: "center"
    },
    uneditable: {
      padding: 5,
      marginBottom: 10
    },
    editable: {
      padding: 5,
      height: 40,
      borderColor: "#000",
      borderWidth: 1,
      marginBottom: 10,
      backgroundColor: "white",
      borderRadius: 10,
      marginBottom: 10,
      color: "#000"
    }
  });
  
