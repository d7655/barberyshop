import React,{ useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity
} from "react-native";
import { RSA } from 'react-native-rsa-native';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Loader from './Loader';


function Login(props) {

  const [cargando, setCargando] = useState(false);
  
  const [use, setUser] = useState('');
  const [pass, setPass] = useState('');
  const [data, setData] = useState( 
  [
    { id: 1, value: "admin", correo: 'admin@barberyshop.com', pass: '1234' },
    { id: 1, value: "user", correo: 'sincere@april.biz', pass: '1234' },
    {id: 1, value: "empleado", correo: 'empleado@barberyshop.gmail.com', pass: '1234' }
  ]);
  const [log, setLog] = useState();

  var RSAKey = require('react-native-rsa');
  const bits = 1024;
  const exponent = '10001'; 
  var rsa = new RSAKey();
  var r = rsa.generate(bits, exponent);
  var publicKey = rsa.getPublicString(); 
  var privateKey = rsa.getPrivateString();
  var rsa = new RSAKey();
  rsa.setPublicString(publicKey);

  const _login = async() => {
    
    //var encrypted = rsa.encrypt(pass);
    let user = data.filter((c) => c.correo === use )
    let pass = data.filter((c) =>  c.pass === '1234')
    if(user.length != 0 && pass != 0){
      if(user[0].value == 'admin'){
        setCargando(false)
         props.navigation.navigate('Principal')
      }else if(user[0].value == 'user'){
        setCargando(false)
         props.navigation.navigate('PrincipalC')
      }else if(user[0].value == 'empleado'){
        setCargando(false)
         props.navigation.navigate('PrincipalE')
      }
    
    }else{
      alert('Datos incorrectos')
      setCargando(false)
    }
  }

  return cargando ? <Loader
  mensaje="Iniciado sesion..."/> :(
    <View style={styles.container}>
      <Text style={styles.Welcome}>BarberyShop</Text>

      <TextInput
        autoCapitalize='none'
        keyboardType='email-address' 
        placeholder="Correo"
        underlineColorAndroid="transparent"
        style={styles.TextInputStyleClass}
        onChangeText={setUser}
      />

      <TextInput
         placeholder='Contraseña' 
        secureTextEntry={true}
              
        onChange={val => setPass(val.nativeEvent.text)}
        autoCapitalize='none'
        underlineColorAndroid="transparent"
        style={styles.TextInputStyleClass}
      />

      <TouchableOpacity style={styles.SubmitButtonStyle} activeOpacity={0.5}>
        <Text style={styles.TextStyle} onPress={()=> {
                
          setCargando(true)
          if(pass.length > 0 && use.length > 0){
            if(pass.length !== 0 ){
              if(use.includes('@')){
                _login()
              }else{
                 setCargando(false)
                alert(
                  'ERROR',
                  'Correo inválido'
                  )
  
              }
    
            }else{ 
              setCargando(false)
               alert(
              'ERROR',
              'Su contraseña debe tener al menos 8 caracteres '
              )
    
              
            }
    
    
          }else{
            setCargando(false)
            alert(
              'ERROR',
              'Ingresa tu correo y contraseña'
              )
    
          }
          
        }}> Ingresar</Text>
      </TouchableOpacity>

    </View>
  );
}

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center"
  },

  Welcome: {
    fontSize: 19,
    color: "#000",
    fontWeight: "bold",
    marginBottom: 30,
    letterSpacing: 2
  },

  TextInputStyleClass: {
    height: 50,
    width: "80%",
    padding: 10,
    borderWidth: 1,
    borderColor: "#000",
    borderRadius: 40,
    marginBottom: 10,
    color: "#000"
  },

  SubmitButtonStyle: {
    width: "33%",
    padding: 10,
    backgroundColor: "#65b6a0",
    borderRadius: 50,
    marginTop: 50,
    marginBottom: 30
  },

  TextStyle: {
    color: "#000",
    textAlign: "center",
    letterSpacing: 2
  },

  HyperLink: {
    textDecorationLine: "underline"
  }
});