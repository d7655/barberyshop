import React,{ useEffect, useState } from "react";
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity, ScrollView,Button,Image
  } from "react-native";
  import { LogBox } from 'react-native';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {FontAwesome5} from '@expo/vector-icons';

import AsyncStorage from "@react-native-async-storage/async-storage";
import Loader from './Loader';

import Constants from 'expo-constants';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';

const Tab = createBottomTabNavigator();

export default function RegistrarProducto(props){   
  
  LogBox.ignoreLogs(["Require cycle:"])
  console.disableYellowBox = true;

  const [cargando, setCargando] = useState(false);
  const [data, setData] = useState([])

  
    const [image, setImage] = useState(undefined);

    const [stock, setStock] = useState('');
    const [precio, setprecio] = useState('');
    const [nombre, setnombre] = useState('');

    function onCreate(){
      const id = data.length + 1;
      data.push({ id: id , name : nombre, price: precio, stock : stock, image_link : image })
      
      const saveUser = async () => {
          setCargando(true);
        try {
          await AsyncStorage.setItem('PRODUCTOS', JSON.stringify(data));
          console.log(data);
          props.navigation.navigate('Principal')
        } catch (err) {
          console.log(err);
        }
        setCargando(false);
      };
      saveUser();
    }    
      useEffect(() => {

        const firstLoad = async () => {
            setCargando(true);
            try {    
              const Mdata = JSON.parse(await AsyncStorage.getItem('PRODUCTOS'))
              setData(Mdata);
            } catch (err) {
              console.log(err);
            }
            setCargando(false);
          };
          firstLoad();
        
    },[])

    return cargando ? <Loader
    mensaje="Cargando..."/> :(
        <View style={forms.container}> 
         <FontAwesome5.Button
        name='camera'
        onPress={
          async () => {
          if (Constants.platform.ios || Constants.platform.android) {
              const permisoGaleria = await Permissions.askAsync(Permissions.CAMERA_ROLL);
              if (permisoGaleria.status === 'granted') {
                  try {
                      let imagenGaleria = await ImagePicker.launchImageLibraryAsync({
                          mediaTypes: ImagePicker.MediaTypeOptions.Images,
                          allowsEditing: true,
                          aspect: [4, 3],
                          quality: 1
                      });
                      if (imagenGaleria.cancelled === false) {
                          setImagen(imagenGaleria.uri);
                      }
                      else {
                      }
                  }
                  catch (e) {
                      console.log(e);
                  }
              }
              else {
                  Alert.alert(
                      'ERROR',
                      'No contamos con el permiso para acceder a la galería'
                  )
              }
          }
        }}
        />
        {image && <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
        <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        placeholder='Nombre'
        onChangeText={setnombre}
        />
      
        <TextInput
          style={forms.editable}
          editable
          maxLength={20}
          placeholder='Precio'
          onChangeText={setprecio}
        />
        
        <TextInput
            placeholder="Stock"
            secureTextEntry={true}
            underlineColorAndroid="transparent"
            style={forms.editable}
            onChangeText={setStock}
        />
        <Button  title="Registrar"  onPress={() =>  onCreate()} />
      </View>
      );
    
  }

  const forms = StyleSheet.create({
      container: {
        margin: 10
      },
      alert: {
        margin: 15,
        fontSize: 20,
        fontWeight: "bold",
        textAlign: "center"
      },
      uneditable: {
        padding: 5,
        marginBottom: 10
      },
      editable: {
        padding: 5,
        height: 40,
        borderColor: "#000",
        borderWidth: 1,
        marginBottom: 10,
        backgroundColor: "white",
        borderRadius: 10,
        marginBottom: 10,
        color: "#000"
      }
    });
    
  