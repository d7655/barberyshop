import React,{ useEffect, useState, Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
  Button
} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { useIsFocused } from '@react-navigation/native';

import {FontAwesome5} from '@expo/vector-icons';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Loader from './Loader';

export default function Citas(props){
    const [cargando, setCargando] = useState(false);
    const [data, setData] = useState();
    const isFocused = useIsFocused();

    function onDelete(id){
      setData(data.filter((c) => c.id !== id))
      const dropItem = async () => {
        try {
          await AsyncStorage.setItem('CITASC', JSON.stringify(data.filter((c) => c.id !== id)));
        } catch (err) {
          console.log(err);
        }
      };
      dropItem();
    }


    useEffect(() => {
    
      const firstLoad = async () => {
        setCargando(true);
        try {    
          const Mdata = JSON.parse(await AsyncStorage.getItem('CITASC'))
          setData(Mdata);
        } catch (err) {
          console.log(err);
        }
        setCargando(false);
      };
      firstLoad();
    }, [isFocused]);
  
    return cargando ? <Loader
    mensaje="Cargando..."/> : (
      <ScrollView>
        <View style={lists.fixToButon}>
        <FontAwesome5.Button
              onPress={() => props.navigation.navigate("Registrar cita cliente", { usuario: 'Leanne Graham'})}
              name='plus'
              color="white"
              backgroundColor="green"
              size={25}
              style={buttons.primary}
             />
        </View>
        
        {data?.map((user, index) => (
          <View style={lists.list}  key ={index}>
            <View style={lists.fixToText}>
              <View>
                <Text>Usuario: {user.usuario}</Text>
                <Text>Horario: {user.horario}</Text>
                <Text>servicio: {user.servicio}</Text>
              </View>
            </View>
            <FontAwesome5
                  onPress={() => props.navigation.navigate("Editar cita cliente", {  id: user.id, usuario: user.usuario, horario: user.horario, servicio: user.servicio, fecha: user.fecha })}
                  name='edit'
                  Color='red'
                  size={25}
                />
            <FontAwesome5
                  onPress={() => onDelete(user.id)} 
                  name='trash'
                  backgroundColor="red"
                  size={25}
                  />
            </View>
        ))}
      </ScrollView>
    
    );

}

const styles = new StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#ffff',
      alignItems: 'center',
      justifyContent: 'center',
  },
  Titulo: {
    fontSize: 19,
    color: "#000",
    fontWeight: "bold",
    marginBottom: 30,
    letterSpacing: 2
  },
  title: {
    textAlign: "center",
    marginVertical: 8
  },
  list: {
    flex: 1,
    justifyContent: "space-between",
    flexDirection: "row",
    padding: 10,
    flexWrap: "wrap"
  },
  fixToText: {
    width: "50%"
  },
  fixToButon: {
    width: "20%"

  },
  separator: {
    marginVertical: 8,
    borderBottomColor: "#737373",
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  scrollView: {
    marginHorizontal: 5
  }
});

const forms = StyleSheet.create({
  container: {
    margin: 10
  },
  alert: {
    margin: 15,
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center"
  },
  uneditable: {
    padding: 5,
    marginBottom: 10
  },
  editable: {
    padding: 5,
    height: 5,
    width: 5,
    borderColor: "gray",
    borderWidth: 1,
    marginBottom: 10,
    backgroundColor: "white"
  }
});

const buttons = StyleSheet.create({
  primary: {
    flex: 1,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 5,
    marginRight: 5
  }
});

const lists = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: "left",
    marginHorizontal: 8,
    marginTop: 10,
    backgroundColor: "#ffff"
  },
  title: {
    textAlign: "center",
    marginVertical: 8
  },
  list: {
    flex: 1,
    justifyContent: "space-between",
    flexDirection: "row",
    padding: 10,
    flexWrap: "wrap"
  },
  fixToText: {
    width: "60%"
  },
  separator: {
    marginVertical: 4,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  scrollView: {
    marginHorizontal: 5
  }
});