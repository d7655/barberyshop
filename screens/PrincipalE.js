import React,{ useEffect, useState, Component } from "react";
import { Text, View } from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {FontAwesome5} from '@expo/vector-icons';

const Tab = createBottomTabNavigator();

import CitasE from './CitasE';
import Usuarios from './Usuarios';
import ProductosE from './ProductosE';
import ServiciosE from './ServiciosE';
import Inventario from './Inventario';

export default function PrincipalE(props){
    return(
        <Tab.Navigator
        screenOptions={({route}) => ({
            tabBarIcon :({color}) => {
                let icono;
                if(route.name == 'CitasE'){
                    icono = 'calendar-alt';
                } else if(route.name == 'Usuarios'){
                    icono = 'user-alt';
                } else if(route.name == 'ProductosE'){
                    icono = 'pump-soap';
                }else if(route.name == 'ServiciosE'){
                    icono = 'cut';
                }

                return <FontAwesome5 name={icono} size={20} color={color}/>
            } 
        })
    }
        tabBarOptions={{
            activeTintColor: 'blue',
            inactiveTintColor: 'gray',
            showLabel: false,
        
        }}
        >
            <Tab.Screen
            name='CitasE'
            component={CitasE}
            />
            <Tab.Screen
            name='ProductosE'
            component={ProductosE}
            options={{title: 'Productos'}}
            />
            <Tab.Screen
            name='ServiciosE'
            component={ServiciosE}
            options={{title: 'Servicios'}}
            />
        </Tab.Navigator>
    );

}