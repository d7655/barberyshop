import React,{ useEffect, useState, Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
  Button
} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import AsyncStorage from "@react-native-async-storage/async-storage";
import Loader from './Loader';
import {FontAwesome5} from '@expo/vector-icons';
import { useIsFocused } from '@react-navigation/native';

export default function Productos(props){

  const isFocused = useIsFocused();

  const [cargando, setCargando] = useState(false);
  const [data, setData] = useState([])

  function onDelete(id){
    setData(data.filter((c) => c.id !== id))
    const DeleteItem = async () => {
      try {
        await AsyncStorage.setItem('PRODUCTOS', JSON.stringify(data.filter((c) => c.id !== id)));
      } catch (err) {
        console.log(err);
      }
    };
    DeleteItem()
  }

  useEffect(() => {
    
    const firstLoad = async () => {
      setCargando(true);
      try {    
        const Mdata = JSON.parse(await AsyncStorage.getItem('PRODUCTOS'))
        setData(Mdata);
      } catch (err) {
        console.log(err);
      }
      setCargando(false);
    };
    firstLoad();
  }, [isFocused]);

  return cargando ? <Loader
  mensaje="Cargando..."/> : (
    <View style={styles.container}>
      <View >
       <FontAwesome5.Button
              onPress={() => props.navigation.navigate("Registrar producto")}
              name='plus'
              color="white"
              backgroundColor="green"
              size={15}
              style={buttons.primary}
             />
      </View>
        
      <FlatList style={styles.list}
        contentContainerStyle={styles.listContainer}
        data={data}
        horizontal={false}
        numColumns={2}
        keyExtractor= {(item) => {
          return item.id;
        }}
        ItemSeparatorComponent={() => {
          return (
            <View style={styles.separator}/>
          )
        }}
        key ={(item) => {
          return item.id;
        }}
        renderItem={(post) => {
          const item = post.item;
          return (
            <View style={styles.card}>
             
             <View style={styles.cardHeader}>
                <View>
                  <Text style={styles.title}>{item.name}</Text>
                  <Text style={styles.price}>{item.price}</Text>
                </View>
              </View>

              <Image style={styles.cardImage} source={{uri:item.image_link}}/>
              
              <View style={styles.cardFooter}>
                <View style={styles.socialBarContainer}>
                  <View style={styles.socialBarSection}>
                    <TouchableOpacity style={styles.socialBarButton} onPress={() => props.navigation.navigate("Editar producto", {  id: item.id, name : item.name, price : item.price, image_link: item.image_link})}>
                      <FontAwesome5
                        name='edit'
                        size={25}
                      />
                    </TouchableOpacity>
                  </View>
                  <View style={styles.socialBarSection}>
                    <TouchableOpacity style={styles.socialBarButton} onPress={() => onDelete(item.id)}>
                      <FontAwesome5
                        name='trash'
                        size={25}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          )
        }}/>
    </View>
  );
}

const styles = StyleSheet.create({
container:{
  flex:1,
  marginTop:20,
},
list: {
  paddingHorizontal: 5,
  backgroundColor:"#E6E6E6",
},
listContainer:{
  alignItems:'center'
},
separator: {
  marginTop: 10,
},
/******** card **************/
card:{
  shadowColor: '#00000021',
  shadowOffset: {
    width: 2
  },
  shadowOpacity: 0.5,
  shadowRadius: 4,
  marginVertical: 8,
  backgroundColor:"white",
  flexBasis: '47%',
  marginHorizontal: 5,
},
cardHeader: {
  paddingVertical: 17,
  paddingHorizontal: 16,
  borderTopLeftRadius: 1,
  borderTopRightRadius: 1,
  flexDirection: 'row',
  justifyContent: 'space-between',
},
cardContent: {
  paddingVertical: 12.5,
  paddingHorizontal: 16,
},
cardFooter:{
  flexDirection: 'row',
  justifyContent: 'space-between',
  paddingTop: 12.5,
  paddingBottom: 25,
  paddingHorizontal: 16,
  borderBottomLeftRadius: 1,
  borderBottomRightRadius: 1,
},
cardImage:{
  flex: 1,
  height: 150,
  width: null,
},
/******** card components **************/
title:{
  fontSize:15,
  flex:1,
},
price:{
  fontSize:16,
  color: "green",
  marginTop: 5
},
buyNow:{
  color: "purple",
},
icon: {
  width:25,
  height:25,
},
/******** social bar ******************/
socialBarContainer: {
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'row',
  flex: 1
},
socialBarSection: {
  justifyContent: 'center',
  flexDirection: 'row',
  flex: 1,
},
socialBarlabel: {
  marginLeft: 8,
  alignSelf: 'flex-end',
  justifyContent: 'center',
},
socialBarButton:{
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
}
});  
const buttons = StyleSheet.create({
  primary: {
    flex: 1,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 5,
    marginRight: 5
  }
});

const lists = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: "left",
    marginHorizontal: 8,
    marginTop: 10,
    backgroundColor: "#ffff"
  },
  title: {
    textAlign: "center",
    marginVertical: 8
  },
  list: {
    flex: 1,
    justifyContent: "space-between",
    flexDirection: "row",
    padding: 10,
    flexWrap: "wrap"
  },
  fixToText: {
    width: "60%"
  },
  fixToButon: {
    width: "20%"

  },
  separator: {
    marginVertical: 4,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  scrollView: {
    marginHorizontal: 5
  }
});