import React,{ useEffect, useState, Component } from "react";
import { Text, View } from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {FontAwesome5} from '@expo/vector-icons';

const Tab = createBottomTabNavigator();

import Citas from './Citas';
import Usuarios from './Usuarios';
import Productos from './Productos';
import Servicios from './Servicios';
import Inventario from './Inventario';

export default function Principal(props){
    return(
        <Tab.Navigator
        screenOptions={({route}) => ({
            tabBarIcon :({color}) => {
                let icono;
                if(route.name == 'Citas'){
                    icono = 'calendar-alt';
                } else if(route.name == 'Usuarios'){
                    icono = 'user-alt';
                } else if(route.name == 'Productos'){
                    icono = 'pump-soap';
                }else if(route.name == 'Servicios'){
                    icono = 'cut';
                }else if(route.name == 'Inventario'){
                    icono = 'clipboard-list';
                }

                return <FontAwesome5 name={icono} size={20} color={color}/>
            } 
        })
    }
        tabBarOptions={{
            activeTintColor: 'blue',
            inactiveTintColor: 'gray',
            showLabel: false,
        
        }}
        >
            <Tab.Screen
            name='Citas'
            component={Citas}
            />
            <Tab.Screen
            name='Usuarios'
            component={Usuarios}
            options={{title: 'Usuarios'}}
            />
            <Tab.Screen
            name='Productos'
            component={Productos}
            options={{title: 'Productos'}}
            />
            <Tab.Screen
            name='Servicios'
            component={Servicios}
            options={{title: 'Servicios'}}
            />
            <Tab.Screen
            name='Inventario'
            component={Inventario}
            options={{title: 'Inventario'}}
            />
        </Tab.Navigator>
    );

}