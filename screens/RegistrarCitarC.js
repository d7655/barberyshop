import React,{ useEffect, useState } from "react";
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity, ScrollView,Button
  } from "react-native";

  import AsyncStorage from "@react-native-async-storage/async-storage";
import Loader from './Loader';


export default function RegistrarCitaC(props){
    const [cargando, setCargando] = useState(false);
    const [data, setData] = useState([])
    const [dataC, setDataC] = useState([])
  
    
    const [id, setId] = useState('');
    const [username, setUsername] = useState('');
    const [horario, setHorario] = useState('');
    const [fecha, setfecha] = useState('');
    const [servicio, setServicio] = useState('');

    function onCreate(){
        const id = data.length + 1;
        data.push({ id: id , username : username , horario: horario, fecha : fecha, servicio : servicio })
        dataC.push({ id: id , username : username , horario: horario, fecha : fecha, servicio : servicio })
        
        const saveUser = async () => {
            setCargando(true);
          try {
            await AsyncStorage.setItem('CITAS', JSON.stringify(data));
            await AsyncStorage.setItem('CITASC', JSON.stringify(dataC));
            props.navigation.navigate('PrincipalC')
          } catch (err) {
            console.log(err);
          }
          setCargando(false);
        };
        saveUser();
      }

      
    useEffect(() => {

        setUsername(props.route.params.usuario);
        const firstLoad = async () => {
            setCargando(true);
            try {    
              const Mdata = JSON.parse(await AsyncStorage.getItem('CITAS'))
              const Cdata = JSON.parse(await AsyncStorage.getItem('CITASC'))
              setData(Mdata);setDataC(Cdata);
            } catch (err) {
              console.log(err);
            }
            setCargando(false);
          };
          firstLoad();
        
    },[])

    return cargando ? <Loader
    mensaje="Cargando..."/> :( 
    <View style={forms.container}> 
    
    <Text style={forms.uneditable}>Usuario: {username}  </Text>
      <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        onChangeText={setHorario}
        placeholder={'Horario'}
      />
      <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        onChangeText={setfecha}
        placeholder={'Fecha'}
      />
      <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        onChangeText={setServicio}
        placeholder={'Servicio'}
      />
      <Button  title="Registrar" onPress={() =>  onCreate()}  />
    </View>
    );
  
}

const forms = StyleSheet.create({
    container: {
      margin: 10
    },
    alert: {
      margin: 15,
      fontSize: 20,
      fontWeight: "bold",
      textAlign: "center"
    },
    uneditable: {
      padding: 5,
      marginBottom: 10
    },
    editable: {
      padding: 5,
      height: 40,
      borderColor: "#000",
      borderWidth: 1,
      marginBottom: 10,
      backgroundColor: "white",
      borderRadius: 10,
      marginBottom: 10,
      color: "#000"
    }
  });
  